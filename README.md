# Advanced Microcontrollers

Documentation and example code for the Advanced Microcontrollers course.

 * [Course text](adv-micro.md)
 * [Example sketches](teal_mallet_gsm/examples/)

To install the examples and the teal_mallet library, copy the `teal_mallet`
folder into your Arduino Sketchbook folder (eg. `MyDocuments/Arduino/libraries`).
Then close all open Arduino IDE windows and reopen. The examples will be listed
under `File → Examples → Teal-Mallet`.

## Required Libraries

To run the examples contained in the `teal_mallet` library, you may also need
to install the dependent libraries.

 * `red_peg` library from [git.defproc.co.uk/ea/inter-micro](https://git.defproc.co.uk/inter-micro),
 * `SDFat` from [github.com/greiman/SdFat](https://github.com/greiman/SdFat),
 * `SDI-12` from [github.com/EnviroDIY/Arduino-SDI-12/](https://github.com/EnviroDIY/Arduino-SDI-12/).

For the SDFat library, copy the folder `SdFat` in to the Arduino sketchbook
libraries folder. For the SDI-12 library, rename the dowloaded folder from
`Arduino-SDI-12-master` to `SDI12` and move to the Arduino sketchbook libraries
folder.
