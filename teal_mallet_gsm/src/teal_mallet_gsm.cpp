#include <inttypes.h>
#include "Arduino.h"

#include "teal_mallet_gsm.h"
CountingStream countIt;
GPRS Gprs;
GSM GsmAccess;
GSMClient Client;

//teal_mallet_gsm::teal_mallet_gsm(Client& aClient) :Client(aClient)
//{
//}

void teal_mallet_gsm::begin(char* server, char* deviceId, char* entityId, int port) {
  _server = server;
  _port = port;
  _entityId = entityId;
  _deviceId = deviceId;
  //delay(100);
}

//int teal_mallet_gsm::post(char *entityId, char *deviceId, t_SensorType reading_vars, int one_data)
//{
  //HttpClient http(Client);
//
//}

uint8_t teal_mallet_gsm::connectGPRS(char* _apn, char* _login, char* _passwd, char* _pin)
{
  while (!_gsm_connected) {
    digitalWrite(3,HIGH);
    if(GsmAccess.begin(_pin)==GSM_READY) {
      delay(3000);
      if(Gprs.attachGPRS(_apn, _login, _passwd)==GPRS_READY) {
        _gsm_connected = true;
        return true;
      }
    } else {
      delay(1000);
    }
  }
}

void teal_mallet_gsm::disconnectGPRS()
{
  while(_gsm_connected) {
    if(GsmAccess.shutdown()) {
      delay(1000);
      //digitalWrite(3,LOW);
      _gsm_connected = false;
    } else {
      delay(1000);
    }
  }
}

uint8_t teal_mallet_gsm::startConnection()
{
  if (Client.connect(_server, _port)) {
    _client_connection = true;
    // Make a HTTP request:
    Client.print(F("POST "));
    // path
    Client.print(F("/api/"));
    Client.print(_entityId);
    Client.print(F("/station/"));
    Client.print(_deviceId);
    Client.print(F("/measurements"));
    // http version
    Client.println(F(" HTTP/1.1"));
    // host line
    Client.print(F("Host: "));
    Client.println(_server);
    // required headers:
    Client.println(F("Connection: close"));
    Client.println(F("Content-Type: application/json"));
    return true;
  } else {
    return false;
  }
}

uint8_t teal_mallet_gsm::endConnection()
{
  // if there's a connection, stop the Client:
  if (_client_connection == true) {
    while (Client.available()) {
      Client.read();
    }
    Client.stop();
    return true;
  } else {
    // we're not connected
    return false;
  }
}

int16_t teal_mallet_gsm::sendOneReading(uint16_t _y, uint8_t _m, uint8_t _d, uint8_t _hh, uint8_t _mm, uint8_t _ss, double _float_reading, t_SensorType _sensor_type, bool send_to_serial)
{
  if (_client_connection == true) {
    // build the body into a char array for sending
    char body[300];
    strcpy(body,"{\"devices\":[{");
    strcat(body, "\"deviceId\":\"");
    strcat(body, _deviceId);
    strcat(body, "\",\"entityId\":\"");
    strcat(body, _entityId);
    strcat(body, "\",\"readings\":[");
    strcat(body, "{\"type\":\"");
    strcat(body, _sensor_type.type);
    strcat(body, "\",\"unit\":\"");
    strcat(body, _sensor_type.unit);
    strcat(body, "\",\"period\":\"");
    strcat(body, _sensor_type.period);
    strcat(body, "\"}");
    strcat(body, "],\"measurements\":[");
    strcat(body, "{\"type\": \"");
    strcat(body, _sensor_type.type);
    strcat(body, "\",\"timestamp\":\"");
    char buff[8];
    sprintf(buff, "%04d", _y);
    strcat(body, buff);
    strcat(body, "-");
    sprintf(buff, "%02d", _m);
    strcat(body, buff);
    strcat(body, "-");
    sprintf(buff, "%02d", _d);
    strcat(body, buff);
    strcat(body, "T");
    sprintf(buff, "%02d", _hh);
    strcat(body, buff);
    strcat(body, ":");
    sprintf(buff, "%02d", _mm);
    strcat(body, buff);
    strcat(body, ":");
    sprintf(buff, "%02d", _ss);
    strcat(body, buff);
    strcat(body, "Z");
    strcat(body, "\",\"value\":");
    ftoa(buff, _float_reading, 3);
    strcat(body, buff);
    strcat(body, "}]}]}");

    //calculate content length
    Client.print(F("Content-Length: "));
    Client.print(strlen(body));
    Client.println();
    Client.println();
    Client.println(body);
    Client.println();
    if (send_to_serial == true) {
      Serial.println(body);
    }

    // wait for any incoming data
    unsigned long timeoutStart = millis();
    uint16_t char_counter = 0;
    uint16_t status_code = 0;
    while ( (Client.connected() || Client.available()) && ( (millis() - timeoutStart) < 30000UL) ) {
      if (Client.available()) {
        char c = Client.read();
        char_counter++;
        // Print out this character
        if (send_to_serial == true) {
          Serial.print(c);
        }
        if (char_counter > 9 && char_counter < 13) {
          // TODO: we should probably check that we've got numbers too!
          //chars 10, 11 & 12 form the status_code
          status_code = (status_code * 10) + (c - '0');
        }
        // We read something, reset the timeout counter
        timeoutStart = millis();
      } else {
        // We haven't got any data, so let's pause to allow some to arrive
        delay(1000);
      }
    }
    return status_code;
  } else {
    // if you didn't get a connection to the server:
    return -1;
  }
}

char* teal_mallet_gsm::ftoa(char *a, double f, int precision)
{
 long p[] = {0,10,100,1000,10000,100000,1000000,10000000,100000000};

 char *ret = a;
 long heiltal = (long)f;
 itoa(heiltal, a, 10);
 while (*a != '\0') a++;
 *a++ = '.';
 long desimal = abs((long)((f - heiltal) * p[precision]));
 itoa(desimal, a, 10);
 return ret;
}
