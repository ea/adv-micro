/*
This file is part of the GSM4 communications library for Arduino
-- Multi-transport communications platform
-- Fully asynchronous
-- Includes code for the Arduino-Telefonica GSM/GPRS Shield V1
-- Voice calls
-- SMS
-- TCP/IP connections
-- HTTP basic clients

This library has been developed by Telef�nica Digital - PDI -
- Physical Internet Lab, as part as its collaboration with
Arduino and the Open Hardware Community.

September-December 2012

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

The latest version of this library can always be found at
https://github.com/BlueVia/Official-Arduino
*/
#include "GSM4MobileServerService.h"
#include "GSM4MobileServerProvider.h"
#include "GSM4MobileClientProvider.h"


#define __TOUTSERVER__ 10000
#define BUFFERSIZETWEET 100

#define GSM4MOBILESERVERSERVICE_SYNCH 0x01 // 1: TRUE, compatible with other clients 0: FALSE

// While there is only a shield (ShieldV1) we will include it by default
#include "GSM4ShieldV1ServerProvider.h"
//GSM4ShieldV1ServerProvider theShieldV1ServerProvider;


GSM4MobileServerService::GSM4MobileServerService(uint8_t port, bool synch)
{
	mySocket=0;
	_port=port;
	flags = 0;

	// If synchronous
	if(synch)
		flags |= GSM4MOBILESERVERSERVICE_SYNCH;
}

// Returns 0 if last command is still executing
// 1 if success
// >1 if error
int GSM4MobileServerService::ready()
{
	return theGSM4MobileServerProvider->ready();
}

void GSM4MobileServerService::begin()
{
	if(theGSM4MobileServerProvider==0)
		return;
	theGSM4MobileServerProvider->connectTCPServer(_port);

	if(flags & GSM4MOBILESERVERSERVICE_SYNCH)
		waitForAnswer();
}

GSM4MobileClientService GSM4MobileServerService::available(bool synch)
{
	int newSocket;
	// In case we are debugging, we'll need to force a look at the buffer
	ready();

	newSocket=theGSM4MobileServerProvider->getNewOccupiedSocketAsServer();

	// Instatiate new client. If we are synch, the client is synchronous/blocking
	GSM4MobileClientService client((uint8_t)(newSocket), (flags & GSM4MOBILESERVERSERVICE_SYNCH));

	return client;
}

size_t GSM4MobileServerService::write(uint8_t c)
{
// Adapt to the new, lean implementation
//	theGSM4MobileServerProvider->writeSocket(c);
	return 1;
}

void GSM4MobileServerService::beginWrite()
{
// Adapt to the new, lean implementation
//	theGSM4MobileServerProvider->beginWriteSocket(local1Remote0, mySocket);
}

size_t GSM4MobileServerService::write(const uint8_t* buf)
{
// Adapt to the new, lean implementation
//	theGSM4MobileServerProvider->writeSocket((const char*)(buf));
	return strlen((const char*)buf);
}

size_t GSM4MobileServerService::write(const uint8_t* buf, size_t sz)
{
// Adapt to the new, lean implementation
//	theGSM4MobileServerProvider->writeSocket((const char*)(buf));
}

void GSM4MobileServerService::endWrite()
{
// Adapt to the new, lean implementation
//	theGSM4MobileServerProvider->endWriteSocket();
}

void GSM4MobileServerService::stop()
{

	// Review, should be the server?
	theGSM4MobileClientProvider->disconnectTCP(local1Remote0, mySocket);
	if(flags & GSM4MOBILESERVERSERVICE_SYNCH)
		waitForAnswer();
	theGSM4MobileClientProvider->releaseSocket(mySocket);
	mySocket = -1;
}


/*int GSM4MobileServerService::getIP(char* LocalIP, int LocalIPlength)
{
	return theGSM4MobileServerProvider->getIP(LocalIP, LocalIPlength);
}*/

int GSM4MobileServerService::waitForAnswer()
{
	unsigned long m;
	m=millis();
	int res;

	while(((millis()-m)< __TOUTSERVER__ )&&(ready()==0))
		delay(10);

	res=ready();

	// If we get something different from a 1, we are having a problem
	if(res!=1)
		res=0;

	return res;
}
