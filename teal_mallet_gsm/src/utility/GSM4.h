/*
This file is part of the GSM4 communications library for Arduino
-- Multi-transport communications platform
-- Fully asynchronous
-- Includes code for the Arduino-Telefonica GSM/GPRS Shield V1
-- Voice calls
-- SMS
-- TCP/IP connections
-- HTTP basic clients

This library has been developed by Telef�nica Digital - PDI -
- Physical Internet Lab, as part as its collaboration with
Arduino and the Open Hardware Community.

September-December 2012

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

The latest version of this library can always be found at
https://github.com/BlueVia/Official-Arduino
*/
#ifndef _GSM4SIMPLIFIERFILE_
#define _GSM4SIMPLIFIERFILE_

// This file simplifies the use of the GSM4 library
// First we include everything.

#include "GSM4CircularBuffer.h"
#include "GSM4MobileCellManagement.h"
#include "GSM4MobileClientService.h"
#include "GSM4MobileNetworkRegistry.h"
#include "GSM4MobileServerService.h"
#include "GSM4ShieldV1AccessProvider.h"
#include "GSM4ShieldV1BandManagement.h"
#include "GSM4ShieldV1ClientProvider.h"
#include "GSM4ShieldV1DataNetworkProvider.h"
#include "GSM4ShieldV1ModemVerification.h"
#include "GSM4ShieldV1CellManagement.h"
#include "GSM4ShieldV1PinManagement.h"
#include "GSM4ShieldV1ScanNetworks.h"
#include "GSM4SMSService.h"
#include "GSM4VoiceCallService.h"

#define GSM GSM4ShieldV1AccessProvider
#define GPRS GSM4ShieldV1DataNetworkProvider
#define GSMClient GSM4MobileClientService
#define GSMServer GSM4MobileServerService
#define GSMVoiceCall GSM4VoiceCallService
#define GSM_SMS GSM4SMSService

#define GSMPIN GSM4ShieldV1PinManagement
#define GSMModem GSM4ShieldV1ModemVerification
#define GSMCell GSM4ShieldV1CellManagement
#define GSMBand GSM4ShieldV1BandManagement
#define GSMScanner GSM4ShieldV1ScanNetworks

#endif
