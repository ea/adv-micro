/*
This file is part of the GSM4 communications library for Arduino
-- Multi-transport communications platform
-- Fully asynchronous
-- Includes code for the Arduino-Telefonica GSM/GPRS Shield V1
-- Voice calls
-- SMS
-- TCP/IP connections
-- HTTP basic clients

This library has been developed by Telefónica Digital - PDI -
- Physical Internet Lab, as part as its collaboration with
Arduino and the Open Hardware Community. 

September-December 2012

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

The latest version of this library can always be found at
https://github.com/BlueVia/Official-Arduino
*/
#include "GSM4MobileClientService.h"
#include "GSM4MobileClientProvider.h"
#include <Arduino.h>

// While there is only a shield (ShieldV1) we will include it by default
#include "GSM4ShieldV1ClientProvider.h"
GSM4ShieldV1ClientProvider theShieldV1ClientProvider;


#define GSM4MOBILECLIENTSERVICE_CLIENT 0x01 // 1: This side is Client. 0: This side is Server
#define GSM4MOBILECLIENTSERVICE_WRITING 0x02 // 1: TRUE 0: FALSE
#define GSM4MOBILECLIENTSERVICE_SYNCH 0x04 // 1: TRUE, compatible with other clients 0: FALSE

#define __TOUTBEGINWRITE__ 10000


GSM4MobileClientService::GSM4MobileClientService(bool synch)
{
	flags = GSM4MOBILECLIENTSERVICE_CLIENT;
	if(synch)
		flags |= GSM4MOBILECLIENTSERVICE_SYNCH;
	mySocket=255;
}

GSM4MobileClientService::GSM4MobileClientService(int socket, bool synch)
{
	// We are creating a socket on an existing, occupied one.
	flags=0;
	if(synch)
		flags |= GSM4MOBILECLIENTSERVICE_SYNCH;
	mySocket=socket;
	theGSM4MobileClientProvider->getSocket(socket);
	
}

// Returns 0 if last command is still executing
// 1 if success
// >1 if error 
int GSM4MobileClientService::ready()
{	
	return theGSM4MobileClientProvider->ready();
}

int GSM4MobileClientService::connect(IPAddress add, uint16_t port) 
{
	if(theGSM4MobileClientProvider==0)
		return 2;
		
	// TODO: ask for the socket id
	mySocket=theGSM4MobileClientProvider->getSocket();

	if(mySocket<0)
		return 2;
	
	int res=theGSM4MobileClientProvider->connectTCPClient(add, port, mySocket);
	if(flags & GSM4MOBILECLIENTSERVICE_SYNCH)
		res=waitForAnswer();
	
	return res;
};

int GSM4MobileClientService::connect(const char *host, uint16_t port)
{

	if(theGSM4MobileClientProvider==0)
		return 2;		
	// TODO: ask for the socket id
	mySocket=theGSM4MobileClientProvider->getSocket();

	if(mySocket<0)
		return 2;
	
	int res=theGSM4MobileClientProvider->connectTCPClient(host, port, mySocket);
	if(flags & GSM4MOBILECLIENTSERVICE_SYNCH)
		res=waitForAnswer();
		
	return res;
}

int GSM4MobileClientService::waitForAnswer()
{
	unsigned long m;
	m=millis();
	int res;
	
	while(((millis()-m)< __TOUTBEGINWRITE__ )&&(ready()==0)) 
		delay(100);
	
	res=ready();

	// If we get something different from a 1, we are having a problem
	if(res!=1)
		res=0;

	return res;
}

void GSM4MobileClientService::beginWrite(bool sync)
{
	flags |= GSM4MOBILECLIENTSERVICE_WRITING;
	theGSM4MobileClientProvider->beginWriteSocket(flags & GSM4MOBILECLIENTSERVICE_CLIENT, mySocket);
	if(sync)
		waitForAnswer();
}

size_t GSM4MobileClientService::write(uint8_t c)
{	
	if(!(flags & GSM4MOBILECLIENTSERVICE_WRITING))
		beginWrite(true);
	theGSM4MobileClientProvider->writeSocket(c);
	return 1;
}

size_t GSM4MobileClientService::write(const uint8_t* buf)
{
	if(!(flags & GSM4MOBILECLIENTSERVICE_WRITING))
		beginWrite(true);
	theGSM4MobileClientProvider->writeSocket((const char*)(buf));
	return strlen((const char*)buf);
}

size_t GSM4MobileClientService::write(const uint8_t* buf, size_t sz)
{
	if(!(flags & GSM4MOBILECLIENTSERVICE_WRITING))
		beginWrite(true);
	for(int i=0;i<sz;i++)
		theGSM4MobileClientProvider->writeSocket(buf[i]);
	return sz;
}

void GSM4MobileClientService::endWrite(bool sync)
{
	flags ^= GSM4MOBILECLIENTSERVICE_WRITING;
	theGSM4MobileClientProvider->endWriteSocket();
	if(sync)
		waitForAnswer();
}

uint8_t GSM4MobileClientService::connected()
{
	if(mySocket==255)
		return 0;
	return theGSM4MobileClientProvider->getStatusSocketClient(mySocket);	 
}

GSM4MobileClientService::operator bool()
{
	return connected()==1;
};

int GSM4MobileClientService::available()
{
	int res;

	// Even if not connected, we are looking for available data
	
	if(flags & GSM4MOBILECLIENTSERVICE_WRITING)
		endWrite(true);

	res=theGSM4MobileClientProvider->availableSocket(flags & GSM4MOBILECLIENTSERVICE_CLIENT,mySocket);
	if(flags & GSM4MOBILECLIENTSERVICE_SYNCH)
		res=waitForAnswer();

	return res;
}

int GSM4MobileClientService::read(uint8_t *buf, size_t size)
{
	int i;
	uint8_t c;
	
	for(i=0;i<size;i++)
	{
		c=read();
		if(c==0)
			break;
		buf[i]=c;
	}
	
	return i;
/* This is the old implementation, testing a simpler one
	int res;
	// If we were writing, just stop doing it.
	if(flags & GSM4MOBILECLIENTSERVICE_WRITING)
		endWrite(true);
	res=theGSM4MobileClientProvider->readSocket(flags & GSM4MOBILECLIENTSERVICE_CLIENT, (char *)(buf), size, mySocket);

	return res;
*/
}

int GSM4MobileClientService::read()
{
	if(flags & GSM4MOBILECLIENTSERVICE_WRITING)
		endWrite(true);
	int c=theGSM4MobileClientProvider->readSocket();
	return c;
}

int GSM4MobileClientService::peek()
{
	if(flags & GSM4MOBILECLIENTSERVICE_WRITING)
		endWrite(true);
	return theGSM4MobileClientProvider->peekSocket(/*mySocket, false*/);
}

void GSM4MobileClientService::flush()
{
	if(flags & GSM4MOBILECLIENTSERVICE_WRITING)
		endWrite(true);
	theGSM4MobileClientProvider->flushSocket(/*mySocket*/);
	if(flags & GSM4MOBILECLIENTSERVICE_SYNCH)
		waitForAnswer();

}

void GSM4MobileClientService::stop()
{
	if(flags & GSM4MOBILECLIENTSERVICE_WRITING)
		endWrite(true);
	theGSM4MobileClientProvider->disconnectTCP(flags & GSM4MOBILECLIENTSERVICE_CLIENT, mySocket);
	theGSM4MobileClientProvider->releaseSocket(mySocket);
	mySocket = 0;
	if(flags & GSM4MOBILECLIENTSERVICE_SYNCH)
		waitForAnswer();
}

