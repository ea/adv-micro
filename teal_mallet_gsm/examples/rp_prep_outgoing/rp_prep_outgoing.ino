#include <red_peg.h>
#include <teal_mallet_gsm.h>
red_peg RP;
teal_mallet_gsm TM;

const char deviceId[] = "00000000-0000-0000-0000-000000000000";
const char entityId[] = "a3b40af1-16b5-4727-bcdc-60b0816a1e7b"; //37chars
const char privacy[] = "public";

const char server[] = "training.teal-mallet.net";
int port = 80; // port 80 is the default for HTTP

#define RECORDING_PERIOD 60000UL // 60 seconds

const t_SensorType tempDatastream = {"Temp:1 min:TMP", "C", "INSTANT"};

t_SensorData temp; // id of the sensor from SensorType array

uint32_t last_record = -RECORDING_PERIOD; // start immediately

// initialize the library instance
GPRS gprs;
GSM gsmAccess;
GSMClient client;

void setup()
{
  Serial.begin(BAUD);
  Serial.println(F("start rp_prep_outgoing"));
  delay(500);
  RP.begin();
}

void loop()
{
  // check if we've reached the next time to record a reading
  if (millis() - last_record >= RECORDING_PERIOD) {
    Serial.println(F("sending"));
    RP.sensorsOn();
    delay(100);
    temp = RP.get(TMP);
    RP.sensorsOff();
    if (temp.sensor == TMP) {
      last_record = millis();
      Serial.println(F("HTTP_REQEST:"));
      // Make a HTTP request:
      Serial.print(F("POST "));
      // path
      Serial.print(F("/api/"));
      Serial.print(entityId);
      Serial.print(F("/station/"));
      Serial.print(deviceId);
      Serial.print(F("/measurements"));
      // http version
      Serial.println(F(" HTTP/1.1"));
      // host line
      Serial.print(F("Host: "));
      Serial.println(server);
      // required headers:
      Serial.println(F("Connection: close"));
      Serial.println(F("Content-Type: application/json"));

      // build the body into a buffer ready to output
      char body[300];
      strcpy(body,"{\"devices\":[{");
      strcat(body, "\"deviceId\":\"");
      strcat(body, deviceId);
      strcat(body, "\",\"entityId\":\"");
      strcat(body, entityId);
      strcat(body, "\",\"readings\":[");
      strcat(body, "{\"type\":\"");
      strcat(body, tempDatastream.type);
      strcat(body, "\",\"unit\":\"");
      strcat(body, tempDatastream.unit);
      strcat(body, "\",\"period\":\"");
      strcat(body, tempDatastream.period);
      strcat(body, "\"}");
      strcat(body, "],\"measurements\":[");
      strcat(body, "{\"type\": \"");
      strcat(body, tempDatastream.type);
      strcat(body, "\",\"timestamp\":\"");
      char buff[8];
      sprintf(buff, "%04d", temp.y);
      strcat(body, buff);
      strcat(body, "-");
      sprintf(buff, "%02d", temp.m);
      strcat(body, buff);
      strcat(body, "-");
      sprintf(buff, "%02d", temp.d);
      strcat(body, buff);
      strcat(body, "T");
      sprintf(buff, "%02d", temp.hh);
      strcat(body, buff);
      strcat(body, ":");
      sprintf(buff, "%02d", temp.mm);
      strcat(body, buff);
      strcat(body, ":");
      sprintf(buff, "%02d", temp.ss);
      strcat(body, buff);
      strcat(body, "Z");
      strcat(body, "\",\"value\":");
      ftoa(buff, RP.degC(temp), 3);
      strcat(body, buff);
      strcat(body, "}]}]}");

      //calculate content length
      Serial.print(F("Content-Length: "));
      Serial.println(strlen(body));
      Serial.println();
      Serial.println(body);
    }
  }
}

char *ftoa(char *a, double f, int precision)
{
 long p[] = {0,10,100,1000,10000,100000,1000000,10000000,100000000};

 char *ret = a;
 long heiltal = (long)f;
 itoa(heiltal, a, 10);
 while (*a != '\0') a++;
 *a++ = '.';
 long desimal = abs((long)((f - heiltal) * p[precision]));
 itoa(desimal, a, 10);
 return ret;
}
