#include <red_peg.h>
#include <teal_mallet_gsm.h>
red_peg RP;
teal_mallet_gsm TM;

// PIN Number
#define PINNUMBER ""

// APN data
#define GPRS_APN       "mobile.o2.co.uk" // replace your GPRS APN
#define GPRS_LOGIN     "o2web"    // replace with your GPRS login
#define GPRS_PASSWORD  "password" // replace with your GPRS password

const char deviceId[] = "00000000-0000-0000-0000-000000000000";
const char entityId[] = "a3b40af1-16b5-4727-bcdc-60b0816a1e7b"; //37chars
const char privacy[] = "public";

// URL, path & port (for example: arduino.cc)
const char server[] = "training.teal-mallet.net";
int port = 80; // port 80 is the default for HTTP
// POST to /api/<entity_id>/station/<id>/measurements

#define RECORDING_PERIOD 60000UL // 60 seconds

const t_SensorType tempDatastream = {"Temp:1 min:TMP", "C", "INSTANT"};

t_SensorData temp; // id of the sensor from SensorType array

uint32_t last_record = -RECORDING_PERIOD; // send data immediately

// initialize the library instance
GPRS gprs;
GSM gsmAccess;
GSMClient client;

void setup()
{
  Serial.begin(BAUD);
  Serial.println(F("start rp_send_temp"));
  delay(500);
  RP.begin();

  // connection state
  boolean notConnected = true;
  // After starting the modem with GSM.begin()
  // attach the shield to the GPRS network with the APN, login and password
  while (notConnected) {
    if ((gsmAccess.begin(PINNUMBER) == GSM_READY) &
        (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY)) {
      notConnected = false;
    } else {
      Serial.println("Not connected");
      delay(1000);
    }
  }

}

void loop()
  // check the existing files and build the next filename

{
  // check if we've reached the next time to record a reading
  if (millis() - last_record >= RECORDING_PERIOD) {
    client.stop();
    Serial.println(F("sending"));
    RP.sensorsOn();
    delay(100);
    temp = RP.get(TMP);
    RP.sensorsOff();
    if (temp.sensor == TMP) {
      Serial.print(temp.y);
      Serial.write('-');
      Serial.print(temp.m);
      Serial.write('-');
      Serial.print(temp.d);
      Serial.write('T');
      Serial.print(temp.hh);
      Serial.write(':');
      Serial.print(temp.mm);
      Serial.write(':');
      Serial.print(temp.ss);
      Serial.print("Z, ");
      Serial.print(RP.degC(temp));
      Serial.println(F(" degC"));
      last_record = millis();

      Serial.println("connecting...");
      if (client.connect(server, port)) {

        Serial.println(F("HTTP_REQ"));
        // Make a HTTP request:
        client.print(F("POST "));
        // path
        client.print(F("/api/"));
        client.print(entityId);
        client.print(F("/station/"));
        client.print(deviceId);
        client.print(F("/measurements"));
        // http version
        client.println(F(" HTTP/1.1"));
        // host line
        client.print(F("Host: "));
        client.println(server);
        // required headers:
        client.println(F("Connection: close"));
        client.println(F("Content-Type: application/json"));

        // build the body into a char array for sending
        char body[300];
        strcpy(body,"{\"devices\":[{");
        strcat(body, "\"deviceId\":\"");
        strcat(body, deviceId);
        strcat(body, "\",\"entityId\":\"");
        strcat(body, entityId);
        strcat(body, "\",\"readings\":[");
        strcat(body, "{\"type\":\"");
        strcat(body, tempDatastream.type);
        strcat(body, "\",\"unit\":\"");
        strcat(body, tempDatastream.unit);
        strcat(body, "\",\"period\":\"");
        strcat(body, tempDatastream.period);
        strcat(body, "\"}");
        strcat(body, "],\"measurements\":[");
        strcat(body, "{\"type\": \"");
        strcat(body, tempDatastream.type);
        strcat(body, "\",\"timestamp\":\"");
        char buff[8];
        sprintf(buff, "%04d", temp.y);
        strcat(body, buff);
        strcat(body, "-");
        sprintf(buff, "%02d", temp.m);
        strcat(body, buff);
        strcat(body, "-");
        sprintf(buff, "%02d", temp.d);
        strcat(body, buff);
        strcat(body, "T");
        sprintf(buff, "%02d", temp.hh);
        strcat(body, buff);
        strcat(body, ":");
        sprintf(buff, "%02d", temp.mm);
        strcat(body, buff);
        strcat(body, ":");
        sprintf(buff, "%02d", temp.ss);
        strcat(body, buff);
        strcat(body, "Z");
        strcat(body, "\",\"value\":");
        ftoa(buff, RP.degC(temp), 3);
        strcat(body, buff);
        strcat(body, "}]}]}");

        //calculate content length
        client.print(F("Content-Length: "));
        client.print(strlen(body));
        client.println();
        client.println();
        client.println(body);
        client.println();
        Serial.println(body);
      } else {
        // if you didn't get a connection to the server:
        Serial.println(F("CONN_FAIL"));
      }
    } else {
      RP.print_data(temp);
    }
  }

  while (client.available()) {
    char c = client.read();
    Serial.print(c);
  }

  // if the server's disconnected, stop the client:
  if (!client.available() && !client.connected()) {
    client.stop();
  }

}

char *ftoa(char *a, double f, int precision)
{
 long p[] = {0,10,100,1000,10000,100000,1000000,10000000,100000000};

 char *ret = a;
 long heiltal = (long)f;
 itoa(heiltal, a, 10);
 while (*a != '\0') a++;
 *a++ = '.';
 long desimal = abs((long)((f - heiltal) * p[precision]));
 itoa(desimal, a, 10);
 return ret;
}
