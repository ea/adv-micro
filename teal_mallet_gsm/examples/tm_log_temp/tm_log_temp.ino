#include <red_peg.h>
#include <teal_mallet_gsm.h>
red_peg RP;
teal_mallet_gsm TM;

// APN data (O2 sim)
#define GPRS_APN       "mobile.o2.co.uk" // replace your GPRS APN
#define GPRS_LOGIN     "o2web"    // replace with your GPRS login
#define GPRS_PASSWORD  "password" // replace with your GPRS password

// teal-mallet server and station settings
#define SERVER_NAME "training.teal-mallet.net"
#define STATION_ID "00000000-0000-0000-0000-000000000000"

// secure header key
#define KEY ""

#define SENDING_RATE 60000 // 60 seconds
unsigned long last_record = -SENDING_RATE; // send data immediately

t_SensorType tempDatastream = {"Temp:1 min:TMP", "C", "INSTANT"};
t_SensorData temp; // for the return from the temperature reading


void setup()
{
  RP.begin();
  TM.begin(SERVER_NAME, STATION_ID);
  delay(300);
  Serial.begin(BAUD);
  Serial.println(F("start tm_log_temp"));
}

void loop()
{
  // check if we've reached the next time to record a reading
  if (millis() - last_record >= SENDING_RATE) {
    // take a reading
    Serial.print(F("Reading: "));
    RP.sensorsOn();
    delay(100);
    temp = RP.get(TMP);
    RP.sensorsOff();

    // then check we have the right data before sending
    if (temp.sensor == TMP) {
      // reset the time we last had correct data
      last_record = millis();

      // print the reading info to the serial monitor
      Serial.print(temp.y);
      Serial.write('-');
      Serial.print(temp.m);
      Serial.write('-');
      Serial.print(temp.d);
      Serial.write('T');
      Serial.print(temp.hh);
      Serial.write(':');
      Serial.print(temp.mm);
      Serial.write(':');
      Serial.print(temp.ss);
      Serial.print("Z, ");
      Serial.print(RP.degC(temp));
      Serial.println(F(" degC"));

      // turn on the GSM modem
      Serial.print(F("Start GSM..."));
      TM.connectGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD);
      Serial.println(F("ON"));

      // start the HTTP connection and send the first headers
      TM.startConnection();

      // send the Content-Length header and the body
      Serial.print(F("Send JSON..."));
      int status_code = TM.sendOneReading(
                          temp.y,
                          temp.m,
                          temp.d,
                          temp.hh,
                          temp.mm,
                          temp.ss,
                          RP.degC(temp),
                          tempDatastream,
                          false
                        );
      if (status_code >= 200 && status_code < 300) {
        // it was fine
        Serial.print(F("OK:"));
      } else {
        Serial.print(F("ERR: "));
      }
      Serial.println(status_code);

      // make sure to close the connection
      TM.endConnection();

      Serial.print(F("Stop GPRS..."));
      TM.disconnectGPRS();
      Serial.println(F("OFF"));
    } else {
      // if if wasn't right, print out what we do have
      RP.print_data(temp);
    }
  }

}
