Red-Peg
-------

Arduino library for communication with the [red-peg shield](https://git.defproc.co.uk/ea/red-peg).

# Functions

To use the library, include it and create the constructor with a name that you
can remember.

```
#include <red_peg.h>
red_peg RP;
```

To use the library, call the `begin` function during `setup()`:

```
RP.begin();
```

This allows a new variable type that stores all the received data from the
red-peg slave controller: `t_SensorData`

And the functions available are:

 * `t_SensorData returnedValue = RP.get(SENSOR_NAME);`
 * `RP.sensorsOn()`
 * `RP.sensorsOff()`

A set of helper functions will work on the `returnedValue` data to give more
useful results:

 * `RP.print_data(returnedValue)`
 * `RP.degC(returnedValue)`
 * `RP.volts(returnedValue)`
 * `RP.mA(returnedValue)`
 * `RP.level(returnedValue, max_level)`

Sensor control is made by asking for one of the sensor values. `SENSOR_NAME`
options are:

 * `RTC` 
 * `ADC1` which is equal to `MA4_20`
 * `ADC2` which is equal to `ANA`
 * `ADC3` which is equal to `TMP`
 * `SDI` (unused)
